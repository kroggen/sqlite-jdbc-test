# LiteSync JDBC


## this was used: (by now)

```
cd ../litesync/sqlite3.27
gcc build_amalgamation_for_testing.c -o build_amalgamation_for_testing
./build_amalgamation_for_testing
cd -
cp litesync-for-testing/* amalgamation/
export JAVA_HOME=/usr
export SQLITE_SOURCE="amalgamation"
```

## for release

```
make native win32 win64 package
```

## for testing

```
make EXTRAFLAGS="-DLITESYNC_FOR_TESTING" native win32 win64 package
```

## optionally

it could clone litesync in the current folder, then 'make amalgamation'

then use:

```
export SQLITE_SOURCE="litesync-for-testing"
```

## note

the cross-compilation is not working with mac64 and others... check


## installing Docker on Ubuntu

```
sudo apt install docker.io
sudo usermod -a -G docker $USER
newgrp docker
```

## problem with maven on Ubuntu

solved with:

```
sudo bash
/usr/bin/printf '\xfe\xed\xfe\xed\x00\x00\x00\x02\x00\x00\x00\x00\xe2\x68\x6e\x45\xfb\x43\xdf\xa4\xd9\x92\xdd\x41\xce\xb6\xb2\x1c\x63\x30\xd7\x92' > /etc/ssl/certs/java/cacerts
/var/lib/dpkg/info/ca-certificates-java.postinst configure
exit
```
